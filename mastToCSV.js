const outbox = require("./outbox.json");
const ObjectsToCsv = require("objects-to-csv");
const download = require("image-downloader");

const userStem = `${YOUR_MASTODON_INSTANCE_DOMAIN}/users/${YOUR_MASTODON_USERNAME}/statuses/`;
const mediaStem = `https://${YOUR_WORDPRESS_DOMAIN}/wp-content/uploads/`;

const buildWpVideo = (url, alt) => {
    return `

<!-- wp:video -->
<figure class="wp-block-video"><video controls src="${url}"></video><figcaption>${alt}</figcaption></figure>
<!-- /wp:video -->`;
};

const buildWpImage = (url, alt) => {
    return `

<!-- wp:image {"sizeSlug":"large"} -->
<figure class="wp-block-image size-large"><img src="${url}" alt="${alt}"/></figure>
<!-- /wp:image -->`;
};

const notReplyOrIncludesUserStem = (i) =>
    !i.object.inReplyTo || i.object.inReplyTo.indexOf(userStem) >= 0;

const formatted = [];

const isAttachmentImage = (attachment) =>
    attachment.mediaType.indexOf("image") >= 0;

const isAttachmentVideo = (attachment) =>
    attachment.mediaType.indexOf("video") >= 0;

const buildContent = (rawObject) => {
    let content = rawObject.object.content;

    rawObject.object.attachment.forEach((a) => {
        if (isAttachmentVideo(a)) {
            content += buildWpVideo(
                mediaStem + a.url.split("/")[a.url.split("/").length - 1],
                a.name
            );
        } else if (isAttachmentImage(a)) {
            content += buildWpImage(
                mediaStem + a.url.split("/")[a.url.split("/").length - 1],
                a.name
            );
        }
    });

    return content;
};

outbox.orderedItems.forEach((item) => {
    if (
        item.type === "Announce" ||
        !notReplyOrIncludesUserStem(item) ||
        !Array.isArray(item.to) ||
        item.to.length !== 1 ||
        item.to[0] !== "https://www.w3.org/ns/activitystreams#Public"
    ) {
        return;
    }

    formatted.push({
        id: item.id,
        post_type: "post",
        post_author: `${YOUR_WORDPRESS_USERNAME}`,
        post_tags: "imported",
        url: item.object.url,
        post_date: item.published,
        post_content: buildContent(item),
        inReplyTo: item.object.inReplyTo,
        post_status: "publish",
    });
});

const buildReplyChains = (posts) => {
    posts.forEach((f) => {
        if (f.inReplyTo) {
            // find either the root post or the last affixed post
            const sourcePostIndex = posts.findIndex(
                (m) =>
                    (typeof m.id === "string" &&
                        m.id.indexOf(f.inReplyTo) >= 0) ||
                    (typeof m.lastReply === "string" &&
                        m.lastReply.indexOf(f.inReplyTo) >= 0)
            );

            if (sourcePostIndex < 0) {
                // this case is hit if it's a self reply but the root post is not self
                f.shouldEliminate = true;
                return;
            }
            const sourcePost = posts[sourcePostIndex];

            sourcePost.post_content += `

<!-- wp:separator -->
<hr class="wp-block-separator"/>
<!-- /wp:separator -->

<!-- wp:paragraph -->
<p><small>The following was posted on ${
                new Date(f.post_date).toLocaleString().split(",")[0]
            } as a reply to the parent post:</small></p>
<!-- /wp:paragraph -->

${f.post_content}`;
            sourcePost.lastReply = f.id;
        }
    });

    const filtered2 = posts.filter((f) => !f.shouldEliminate && !f.inReplyTo);
    filtered2.forEach((fil) => {
        delete fil.inReplyTo;
        delete fil.url;
        delete fil.id;
        // uncomment the below stuff if you want to prepend a disclaimer
        // to your posts that they were imported from mastodon
//         fil.post_content = `<!-- wp:paragraph -->
// <p><small>The following was originally posted on mastodon:</small></p>
// <!-- /wp:paragraph -->

// <!-- wp:separator -->
// <hr class="wp-block-separator"/>
// <!-- /wp:separator -->

// ${fil.post_content}`;
    });
    return filtered2;
};

const csv = new ObjectsToCsv(buildReplyChains(formatted));

// Save to file:
csv.toDisk("./forIngestIntoWordpress.csv").then((a) => {
    console.log("finished exporting to ./forIngestIntoWordpress.csv");
});
